# Timelog

* Abnormal Infant Movement Detection from 2D Skeletal Data
* James McClure
* 2385464M
* Edmond S. L Ho


## Week 1

### 30 Sep 2022

* *0.5 hour* Not sure if this counts, but had 30min meeting with supervisor to discuss project

### 1 Oct 2022


* *0.5 hour* Created GitLab repository and cloned the cookiecutter for the projects. 


### 2 Oct 2022

* *1 hour* Reviewed meeting recording, set up users stories, 
MOSCOW rankings of task. 
* Started to plan what I needed to do for project in order to 
start programing 

### 3 Oct 2022

* *1 hour* Review through papers on experiment, and spent time 
learning PyTorch to start implementing application

### 5th Oct 2022
* *10 minutes* Further detailing plan of project, 

### 6th Oct 2022
* *2 hours* Studying DL, thinking of questions to ask, studying 
papers to help with production of paper

### 11th Oct 2022
* *2 hours* Looked into methods of doing anomaly detection 
methods, starting to gain basis of what I need to 
investigate/implement

### 12 Oct 2022
* *1 hours* DId more research into anomaly detection methods 

### 18 Oct 2022
* *2 hours* Spent time summarising the whole development path for application
checking with supervisor to ensure that it looks good, and can then start
research on how to implement all algorithms, process data 

### 19 Oct 2022
* *1 hour* Looked into different deep learning algorithms, looked
into more anomaly detection approaches. Tried to understand how
to implement SVM in python

### 22 Oct 2022

* *1 hour* Looked over previous meeting, starting to potentially 
implement extraction features on python
*  *2-3 hours* Did revision over Machine learning concepts this
weekend which will help with my project.
*  *3-4 hours* Did around 3-4 of similar work to the above 
throughout week. 

## Week 6

# 23 Oct 2022
 * *1 hour* Worked on how to implement extraction features, etc..Se.

# 24 Oct 2022
* *1 hour* Setting up framework for application, starting to understand and implement SVM algorithm to start
process. 

# 25 Oct 2022

* *0.5 hours* Meeting 5

# 26 Oct 2022

* *4 hours* Reviewed January paper more, analysed code and downloaded matlab code from January paper
did more research into implementing machine learning algorithms

# 27 Oct 2022
* *4-6 hours* Worked on Machine learning course which will hel in implementation of concepts for
project

## Week 7 

# 31 Oct 2022
* *1 hour*  Analyzed previous meetings for information, investigated matlab code some more,
Researched how to implement and to find any semi-supervised OCSVM algorithms

# 2 Nov 2022
* *1 hour* Did more research into investigation novelty detection methods

# 4 Nov

* *1 hour *