# Plan

* Abnormal Infant Movement Detection from 2D Skeletal Data
* James McClure
* 2385464M
* Edmond S. L Ho

Week-by-week plan for the whole project. Update this as you go along.

## To be Completed in Semester 1
* Project status report
* An initial version of the project product

## To be Completed in Semester 2
* Evaluation of the product, and a complete dissertation 
* presentation
* evaluation analysis. 



## Winter semester

**Week 1**
* Understand what I need to do for project
* Have meeting with supervisor

**Week 2**
* Make user stories of project, fully define objectives and 
get a basis for what I need to do for project.

**Week 3**
* 
**Week 4**

**Week 5**

**Week 6**

**Week 7**

**Week 8**

**Week 9**

**Week 10**

**Week 11 [PROJECT WEEK]**

**Week 12 [PROJECT WEEK]** Status report submitted.

## Winter break

## Spring Semester

* **Week 13**
* **Week 14**
* **Week 15**
* **Week 16**
* **Week 17**
* **Week 19**
* **Week 20**
* **Week 21**
* **Week 22**
* **Week 23 [TERM ENDS]**
* **Week 24** Dissertation submission deadline and presentations.

