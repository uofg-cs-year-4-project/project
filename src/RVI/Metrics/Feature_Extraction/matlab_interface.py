import matlab.engine
import scipy

# eng = matlab.engine.start_matlab()
# eng.A_Feature_Extraction_RVI(nargout = 0)

# Below is the
RVI_38_AngDisp_8bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_AngDisp_8bins.mat")
RVI_38_AngDisp_16bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_AngDisp_16bins.mat")
RVI_38_FFT_JD_8bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_FFT_JD_8bins.mat")
RVI_38_FFT_JD_16bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_FFT_JD_16bins.mat")
RVI_38_FFT_JO_8bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_FFT_JO_8bins.mat")
RVI_38_FFT_JO_16bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_FFT_JO_16bins.mat")
RVI_38_HOJD2D_8bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_HOJD2D_8bins.mat")
RVI_38_HOJD2D_16bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_HOJD2D_16bins.mat")
RVI_38_HOJO2D_8bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_HOJO2D_8bins.mat")
RVI_38_HOJO2D_16bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_HOJO2D_16bins.mat")
RVI_38_Rel_JO_8bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_Rel_JO_8bins.mat")
RVI_38_Rel_JO_16bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_Rel_JO_16bins.mat")
RVI_38_Rel_JO_AngDis_8bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_Rel_JO_AngDis_8bins.mat")
RVI_38_Rel_JO_AngDis_16bins = scipy.io.loadmat("00_25J_RVI_38_Full\\25J_RVI_38_Rel_JO_AngDis_16bins.mat")
RVI_38_interpPoseData = scipy.io.loadmat("00_25J_RVI_38_Full\\A_25J_RVI_38_interpPoseData.mat")
RVI_38_correctedPoseData = scipy.io.loadmat("00_25J_RVI_38_Full\\B_25J_RVI_38_correctedPoseData.mat")
RVI_38_ProcessedData = scipy.io.loadmat("00_25J_RVI_38_Full\\C_25J_RVI_38_ProcessedData.mat")

# eng.exit()
